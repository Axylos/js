(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});
  
  var Game = Hanoi.Game = function Game() {
    this.towers = [ [3, 2, 1], [], [] ];
  };
  
  Game.prototype.run = function() {
    getInput(true, function() {
      console.log("You Won!");
    });
  };
   
})(this);