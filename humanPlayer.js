(function (root) {
  
  function HumanPlayer(sym) {
    this.sym = sym;
  }
  
  HumanPlayer.prototype.promptUser = function(callback) {
    var readline = require('readline');

    READER = readline.createInterface( {
      input: process.stdin,
      output: process.stdout  
    });
    
    READER.question("Enter a move", function(response) {
      var moveArray = response.split(', ');
      var square = {
        row: moveArray[0],
        col: moveArray[1]
      }
      
      callback();
      
    });
    
  }
  
  HumanPlayer.prototype.getMove = function(board) {
    board.render();
    this.promptUser(function() {
      board.markSquare(square, this.sym)
    })
    
  }
  
  this.HumanPlayer = HumanPlayer;
})(this);