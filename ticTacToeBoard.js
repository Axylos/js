(function (root) {
  //var Board1 = root.Board = (root.Board || {});
  
  
  Board.prototype.buildGrid = function(){
    var grid = []
    for (var i = 0; i < 3; i++) {
      var row = [];
      for (var j = 0; j < 3; j++) {
        row.push(null);
      }
      grid.push(row);
    }
    console.log(grid.length);
    return grid;
  };
  
  function Board() {
    this.grid = this.buildGrid();
    this.game = null;
  };
  
  c = new Board();
  
  Board.prototype.setGame = function(game) {
    this.game = game;
  };
  
  
  
  var markSquare = Board.prototype.markSquare = function(square, sym) {
    if(this.grid[square.row][square.column] === null) {
      this.grid[square.row][square.column] = sym;
      this.game.run(sym, true);
    }
    else {
      this.game.run(sym, false);
    }
  };
 
  Board.prototype.checkRow = function(board, xRow, oRow) {
    for(var i = 0; i < 2; i++) {
      if (board[i] == xRow){
        return 'x';
      }
      else if (board[i] == oRow) {
        return 'o';
      }
      else {
        return false;
      }
    }
  }
  
  Board.prototype.checkColumn = function(xRow, oRow) {
    newGrid = [];
    for(var i = 0; i < 2; i++) {
      newRow = []
      for(var j = 0; j < 2; j++) {
        newRow.push(this.grid[j][i])
      }
      newGrid.push(newRow);
    }
    return Board.checkRow(newGrid, xRow, oRow);
  }
  
  Board.prototype.checkDiag = function(xRow, oRow) {
    
    var diag1 = [this.grid[0][0],this.grid[1][1],this.grid[2][2]];
    var diag2 = [this.grid[2,0],this.grid[1][1],this.grid[0][2]];
    
    if(diag1 === xRow || diag2 === xRow) {
      return 'x';
    }
    else if (diag1 === oRow || diag2 === oRow) {
      return 'o';
    }
    else {
      return false;
    }
  }
  
  Board.prototype.checkIfWon = function() {
    var xRow = ["x", "x", "x"]; 
    var oRow = ["o", "o", "o"];
    return checkRow(this.grid, xRow, oRow) || checkColumn(xRow, oRow) || checkDiag(xRow, oRow);
  }
  
  Board.prototype.render = function() {
    console.log(this.grid[0] + '\n');
    console.log(this.grid[1] + '\n');
    console.log(this.grid[2] + '\n');
  }
  
  b = new Board();
  
  root.board = Board;
})(this);