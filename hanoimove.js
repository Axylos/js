(function (root) {
  var Hanoi = root.Hanoi = (root.Hanoi || {});

  var parseMove = Hanoi.parseMove = function(input) {
    var moveArray = input.split(", ");
    return {
      from: Game.towers[moveArray[0]],
      to: Game.towers[moveArray[1]]
    };
  }

  var validMove = Hanoi.validMove = function(move) {
    if (move.from.length === 0) {
      return false;
    }
    else if(move.from[move.from.length - 1 ] > move.to[move.to.length - 1]) {
      return false;
    }
    else {
      return true;
    }
  }

  var performMove = Hanoi.performMove = function(move) {
    if(validMove(move)) {
      piece = move.from.pop();
      move.to.push(piece);
    }
    else {
      console.log("Invalid move")
    }
  }

})(this);