"use strict";

function Clock() {
  var currentTime = {};
  var seconds = 0;
  var minutes = 0;
  var hours = 0;
}

Clock.prototype.display = function() {
 
  console.log(this.hours + ':' + this.minutes + ':' + this.seconds);
}


Clock.prototype.updateClock = function() {
   
  this.seconds += 5;
  
  if (this.seconds > 59) {
    this.minutes++;
    this.seconds -= 60;
  }
  if (this.minutes > 59) {
    this.hours++;
    this.minutes = 0;
  }
  if (this.hours > 23) {
    this.hours = 0;
  }
  
  this.display();
}

Clock.prototype.run = function() {
  var that = this;
  var currentTime = new Date();
  this.seconds = currentTime.getSeconds();
  this.minutes = currentTime.getMinutes();
  this.hours = currentTime.getHours();
  
  setInterval(that.updateClock.bind(that), 5000);
}

var clock = new Clock();

clock.run();

