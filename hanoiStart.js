var towers = require("./towersHanoi.js");
var hanoimove = require("./hanoimove.js");

var readline = require('readline');

READER = readline.createInterface( {
  input: process.stdin,
  output: process.stdout  
});

var Hanoi = root.Hanoi = (root.Hanoi || {});

var Game = Hanoi.Game = function Game() {
  this.towers = [ [3, 2, 1], [], [] ];
};

Game.prototype.run = function() {
  var that = this;
  this.getInput(true, function() {
    console.log("You Won!");
    that.renderTowers();
  });
};

var gameOver = Hanoi.Game.prototype.gameOver = function() {
  if(this.towers[1].length === 3 || this.towers[2].length === 3) {
    return true;
  } 
  else {
    return false;
  }
}

var renderTowers = Hanoi.Game.prototype.renderTowers = function() {
  console.log(this.towers);
}

var getInput = Hanoi.Game.prototype.getInput = function(playing, callback) {
  if (playing) {
    that = this;
    this.renderTowers();
    READER.question("Enter move: ", function(moveInput) {
      var move = that.parseMove(moveInput);
      that.performMove(move);
      playing = !that.gameOver();
      that.getInput(playing, callback);
    });
  }
  else {
    callback();
  }
  

}
var parseMove = Hanoi.Game.prototype.parseMove = function(input) {
  var moveArray = input.split(", ");
  return {
    from: this.towers[moveArray[0]],
    to: this.towers[moveArray[1]]
  };
}

var validMove = Hanoi.Game.prototype.validMove = function(move) {
  if (move.from === undefined || move.to === undefined) {
    return false
  }
  else if (move.from.length === 0) {
    return false;
  }
  else if (move.to.length === 0) {
    return true;
  }
  else if(move.from[move.from.length - 1 ] > move.to[move.to.length - 1]) {
    return false;
  }
  else {
    return true;
  }
}

var performMove = Hanoi.Game.prototype.performMove = function(move) {
  if(this.validMove(move)) {
    piece = move.from.pop();
    move.to.push(piece);
  }
  else {
    console.log("Invalid move")
  }
}
g = new Game();
g.run();