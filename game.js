(function (root) {
  
  function Game(board, player1, player2) {
    this.board = board;
    this.board.setGame(this);
    this.player1 = player1;
    this.player2 = player2;
  }
  
  Game.prototype.run = function(lastPlayerSym,validMove) {
    
    
    if(validMove) {
      currentPlayer = lastPlayerSym === player1.sym ? player2 : player1;
    }
    else {
      console.log("Invalid move");
    }
    
    if(!this.board.checkIfWon) {
      
      currentPlayer.getMove(this.board);
    }
    else {
      console.log(this.checkIfWon + " wins!");
      this.board.renderBoard;
    }
  }
  
  this.game = Game;
})(this);