var readline = require('readline');

READER = readline.createInterface( {
  
  input: process.stdin,
  output: process.stdout
  
});


function addNumbers(sum, numsLeft, completionCallback) {
  if (numsLeft > 0) {
  
    READER.question("Number to add: ", function(number){
      var num = parseInt(number);
      sum += num;
      numsLeft--;
      console.log(sum);
      addNumbers(sum, numsLeft, completionCallback);
    });
    
  } else {
    completionCallback(sum);
  }
  
}

addNumbers(0, 4, function (sum) {
  console.log("Total Sum: " + sum);
});